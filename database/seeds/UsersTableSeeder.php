<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        factory(App\User::class, 10)->create()->each(function ($u) use ($faker) {
            App\Match::create([
                'title' => $faker->sentence(),
                'user_1' => rand(1, 10),
                'user_2' => rand(1, 10),
                'winner_id' => rand(1, 10),
                'completed_at' => \Carbon\Carbon::now(),
            ]);
        });
    }
}
