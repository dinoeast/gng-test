# GoNow Gaming Laravel API Code Test

Welcome to the GoNow Gaming code test! There is nothing scary here, just a short fairly simple code test
to show us your PHP and Laravel skills.

## Expectations

There is more than one solution to this challenge, this is not a trick. It is our way of seeing how
you approach the task. As a tech team we value simple and elegant solutions over complex or verbose
solutions. We expect to see PSR-4 namespaces and PSR-2 code style, well named variables and white
space for readability.

## The Challenge

In this repository there is a database migration the sets up and populates some tables with sample
data of users with some match data. We would like you to craft a simple read-only API that shows
this data in a JSON formatted response.

### Getting Started

As mentioned this is a standard Laravel install so you can use your own setup to run it locally,
whether than be Homestead, Valet, Docker, or the built in Artisan Serve. Copy the `.env.example` file to
`.env` and update according to your setup. You will also need to `composer install` to bring in
the dependencies. Finally run the database migrations `php artisan migrate` and you will be all
set.

### API Structure

We would like you to create the following endpoints:

* `GET /users` should return a collection of user in a JSON formatted response.
* `GET /user/5` should return user 5 with their 10 most recent participated in matches as a relationship.
                User can be user_1 OR user_2

Each user object in the response should contain the following data:

JSON Attribute | JSON Type | Database Column | Special Formatting
---------------|-----------|-----------------|-------------------
id             | int       | id              | none
username       | string    | username        | none
balance        | string    | balance         | Formatted money e.g. "£1,235.23", the database column is pence (£1 == 100)
ranking        | int       | ranking         | Prefix with + if positive and - if negative (e.g. +1256 or -123)
registered     | date      | created_at      | show as ISO-8601
matches        | array     | -               | Matches relationship

Each match object in the response should contain the following data:

JSON Attribute | JSON Type | Database Column | Special Formatting
---------------|-----------|-----------------|-------------------
id             | int       | id              | none
has_winner     | string    | username        | none
started_at     | date      | started_at      | show as ISO-8601
ended_at       | date      | ended_at        | show as ISO-8601
is_complete    | boolean   | completed_at    | 1 or 0

**Important Notes**

* Only users that have `active` set to `1` should be returned in each endpoint
* In a match the user can be user_1 OR user_2
