<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Match extends Model
{
    /**
     * Format the `started_at` field.
     *
     * @param  date  $value
     * @return string
     */
    public function getStartedAtAttribute($value)
    {
        return Carbon::parse($value)->toIso8601String();
    }

    /**
     * Format the `ended_at` field.
     *
     * @param  date  $value
     * @return string
     */
    public function getEndedAtAttribute($value)
    {
        return Carbon::parse($value)->toIso8601String();
    }
}
