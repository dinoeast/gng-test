<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Carbon\Carbon;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * Format the `balance` field.
     *
     * @param  int  $value
     * @return string
     */
    public function getBalanceAttribute($value)
    {
        setlocale(LC_MONETARY, 'en_GB.UTF-8');
        $formattedValue = money_format('%n', $value / 100);
        return htmlentities($formattedValue);
    }

    /**
     * Format the `ranking` field.
     *
     * @param  int  $value
     * @return string
     */
    public function getRankingAttribute($value)
    {
        return $value >= 0 ? ('+' . $value) : (string)$value;
    }

    /**
     * Format the `registered` field.
     *
     * @param  date  $value
     * @return string
     */
    public function getRegisteredAttribute($value)
    {
        return Carbon::parse($value)->toIso8601String();
    }

    /**
     * Filter out inactive users.
     */
    private function filterInactive()
    {
        return $this->where('active', 1);
    }

    /**
     * Define the relationship between `users` table and `matches` table.
     */
    private function matches()
    {
        return $this
            ->hasMany('App\Match', 'user_1')
            ->orWhere('user_2', $this->id);
    }

    /**
     * Get the matches in which the user participated.
     */
    private function getUserMatches($user)
    {
        $matches = $this
            ->find($user->id)
            ->matches()
            ->select(
                'matches.id',
                'matches.created_at as started_at',
                'matches.completed_at as ended_at'
            )
            ->get();

        return $matches;
    }

    /**
     * Get all users.
     */
    public function getUsers()
    {
        $users = $this
            ->filterInactive()
            ->select(
                'users.id',
                'users.username',
                'users.balance',
                'users.ranking',
                'users.created_at as registered'
            )
            ->get();

        foreach ($users as $user) {
            $matches = $this->getUserMatches($user);
            $user->matches = $matches;
        }

        return $users;
    }

    /**
     * Get a single user and the corresponding matches.
     */
    public function getUser($id)
    {
        $user = $this->find($id);

        if ( ! $user) {
            return null;
        }

        $matches = $this->getUserMatches($user);
        $user['matches'] = $matches;

        return $user;
    }
}
