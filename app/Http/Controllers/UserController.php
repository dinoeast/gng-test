<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->user = new User;
    }

    /**
     * Get all active users and their matches.
     *
     * @return \Illuminate\Http\Response
     */
    public function getUsers()
    {
        return $this->user->getUsers();
    }

    /**
     * Get a single user and the corresponding matches.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getUser($id)
    {
        $user = $this->user->getUser($id);

        if ( ! $user) {
            return response()->json([
                'message' => 'User not found',
            ], 404);
        }

        return $user;
    }
}
